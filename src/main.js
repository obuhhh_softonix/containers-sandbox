import Vue from 'vue'
import App from './App.vue'
import ContainersPlugin from '@/containersPlugin'

Vue.config.productionTip = false
Vue.use(ContainersPlugin, {
  peoples: { key: 'id' }
})

export default new Vue({
  el: '#app',
  render: h => h(App),
  mounted () {
    // for testing purposes
    window.container = this.$container
  }
})
