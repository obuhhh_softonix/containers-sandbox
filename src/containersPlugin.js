// TODO: handle change key in entity

let unique = (e, i, a) => a.indexOf(e) === i

function objectByArrayOfKeys (obj, keys) {
  let result = {}
  keys.forEach(key => {
    if (obj.hasOwnProperty(key)) {
      result[key] = obj[key]
    }
  })
  return result
}

class Container {
  constructor (key, app, sortBy) {
    this.key = key
    this.app = app
    this.sortBy = sortBy

    this.groups = {}
    this.entities = {}
  }

  hasGroup (name) {
    return name in this.groups
  }

  arrayFromEntities (entitiesObject) {
    if (entitiesObject) {
      let entitiesList = Object.values(entitiesObject)
      if (this.sortBy !== undefined) {
        let allFieldsHasSortKey = entitiesList.every(entity => entity.hasOwnProperty(this.sortBy))
        if (allFieldsHasSortKey) {
          entitiesList = entitiesList.sort((a, b) => b[this.sortBy].localeCompare(a[this.sortBy]))
        }
      }
      return entitiesList
    }
    return false
  }

  set (entities, group) {
    if (!entities) { throw new Error("any arguments hasn't passed") }
    let isOneEntity = !Array.isArray(entities) && (typeof entities === 'object')

    if (isOneEntity) {
      if (this.key in entities) {
        entities = [entities]
      } else {
        throw new Error('an entity hasn\'t required key')
      }
    }

    let result = {}
    entities.forEach(entity => {
      let key = String(entity[this.key])
      this.app.$set(this.entities, key, entity)
      result[key] = this.entities[key]
    })
    if (typeof group === 'string') {
      this.app.$set(this.groups, group, this.groups[group] || [])
      this.groups[group] = [...this.groups[group], ...Object.keys(result)].filter(unique)
    }

    return isOneEntity ? Object.values(result)[0] : this.arrayFromEntities(result)
  }

  get (...args) {
    let filter = args[0]
    let isArray = Array.isArray(filter)

    // return all entities if arguments hasn't passed
    if (args.length === 0) {
      return this.arrayFromEntities(this.entities)
    }

    if (isArray) {
      filter = filter.map(String)
      let entities = objectByArrayOfKeys(this.entities, filter)
      return this.arrayFromEntities(entities)
    } else {
      filter = String(filter)
      if (filter in this.entities) {
        return this.entities[filter]
      } else {
        return null
      }
    }
  }

  remove (filter, group) {
    let fromGroup = this.hasGroup(group)
    let isArray = Array.isArray(filter)

    // delete just in a group
    if (fromGroup) {
      filter = (Array.isArray(filter) ? filter : [filter]).map(String)
      let filtered = this.groups[group].filter(key => !filter.includes(key))

      this.app.$set(this.groups, group, filtered)
      return true
    }

    // if need to delete globally, unlike just in a group
    if (isArray) {
      filter.map(String).forEach(key => { if (key in this.entities) delete this.entities[key] })
    } else {
      filter = String(filter)
      if (filter in this.entities) delete this.entities[filter]
    }

    this.app.$set(this, 'entities', { ...this.entities })
    return true
  }

  group (name) {
    if (this.hasGroup(name)) {
      return this.arrayFromEntities(objectByArrayOfKeys(this.entities, this.groups[name]))
    } else {
      return []
    }
  }

  clear (group) {
    if (this.hasGroup(group)) {
      this.app.$set(this.groups, group, [])
    } else {
      this.app.$set(this, 'groups', {})
      this.app.$set(this, 'entities', {})
    }
  }
}

let containersList = {}

function createContainer (name, key, app, sortBy) {
  let container = new Container(key, app, sortBy)
  app.$set(containersList, name, container)
  return containersList[name]
}

const ContainersPlugin = {
  install (Vue, initialContainers) {
    containersList = Vue.observable(containersList)

    Vue.mixin({
      created () {
        for (let name in initialContainers) {
          if (!(name in containersList)) {
            let data = initialContainers[name]
            createContainer(name, data.key, this, data.sortBy)
          }
        }
      },
      computed: {
        $container () {
          return function (name) {
            if (name in containersList) {
              return containersList[name]
            }
            return null
          }
        }
      }
    })
  }
}

export default ContainersPlugin
