# containers-plugin-sanbox
Demo: https://containers-sanbox.netlify.com

## Setup
```
import Vue from 'vue'
import ContainersPlugin from '@/containersPlugin'

Vue.use(ContainersPlugin, {
  peoples: { key: 'id' }
})
```

| Option | Type |   | Decsription |
|---|---|---|---|
| id | String | required | Primary unique field in your entities |
| sortBy | String | optional | Key for sorting entities list |


# API reference

## Setting entities
Set (and return reactive entities) to container 
```
.set(filter, [groupName])
```

| Option | Type |   | Decsription |
|---|---|---|---|
| filter | Object/Array | required | Object or array of object witch contains primary fields |
| groupName | String | optional | String value for assigning specific entities to group |

## Getting entities
Return all entities from container if filter has not passed
```
.get([filter])
```

| Option | Type |   | Decsription |
|---|---|---|---|
| filter | Object/Array | optional | Object or array of primary keys |


## Getting by group
```
.group(groupName)
```

| Option | Type |   | Decsription |
|---|---|---|---|
| groupName | String | required | Return array of entities assigned to the group |


## Removing entities
Delete some entity globally or entities from container or just unassign this from group.
```
.remove(filter, [groupName])
```

| Option | Type |   | Decsription |
|---|---|---|---|
| filter | Object/Array | required | Object or array of object witch contains primary fields |
| groupName | String | optional | String value for unassigning specific entities from group |

## Clearing totally
Delete totally all (or assigned to specific group) entities from container
```
.clear([groupName])
```

| Option | Type |   | Decsription |
|---|---|---|---|
| groupName | String | optional | String value for deleting specific group entities from container |

# Playground
Also you can play with it in console
```
> container('peoples').remove(2)
true
```

# Example

```
let peoples = this.$container('peoples') // Get container instance

let peoplesData = [
  { id: 1, name: 'John Cina', money: '100' },
  { id: 2, name: 'Elon Mask', money: '9400' },
  { id: 3, name: 'Jina Ross', money: '450' },
  { id: 4, name: 'Riley Reid', money: '540' },
  { id: 5, name: 'Sasha Grey', money: '1500' }
]

let pornStars = [
  peoplesData[3],
  peoplesData[4]
]

let reactivePeoplesData = peoples.set(peoplesData)
let reactivePornStars = peoples.set(pornStars, 'porn')

reactivePornStars.forEach(p => {
  p.money = p.money * 2
})

console.log(reactivePornStars)
    // [
    //     { id: 4, name: 'Riley Reid', money: '1080' },
    //     { id: 5, name: 'Sasha Grey', money: '3000' }
    // ]

console.log(reactivePeoplesData)
    // [
    //     { id: 1, name: 'John Cina', money: '100' },
    //     { id: 2, name: 'Elon Mask', money: '9400' },
    //     { id: 3, name: 'Jina Ross', money: '450' },
    //     { id: 4, name: 'Riley Reid', money: '1080' },
    //     { id: 5, name: 'Sasha Grey', money: '3000' }
    // ]

console.log(peoples.get(1))
    // { id: 1, name: 'John Cina', money: '100' }

console.log(peoples.get([2, 4]))
    //     { id: 2, name: 'Elon Mask', money: '9400' },
    //     { id: 4, name: 'Riley Reid', money: '1080' },

console.log(peoples.group('porn'))
    // [
    //     { id: 4, name: 'Riley Reid', money: '1080' },
    //     { id: 5, name: 'Sasha Grey', money: '3000' }
    // ]

peoples.remove(4, 'porn')
console.log(peoples.group('porn'))
    // [
    //     { id: 5, name: 'Sasha Grey', money: '3000' }
    // ]

console.log(peoples.get())
    // [
    //     { id: 1, name: 'John Cina', money: '100' },
    //     { id: 2, name: 'Elon Mask', money: '9400' },
    //     { id: 3, name: 'Jina Ross', money: '450' },
    //     { id: 4, name: 'Riley Reid', money: '1080' },
    //     { id: 5, name: 'Sasha Grey', money: '3000' }
    // ]

peoples.remove(5)
console.log(peoples.group('porn'))
    // []

console.log(peoples.get())
    // [
    //     { id: 1, name: 'John Cina', money: '100' },
    //     { id: 2, name: 'Elon Mask', money: '9400' },
    //     { id: 3, name: 'Jina Ross', money: '450' },
    //     { id: 4, name: 'Riley Reid', money: '1080' }
    // ]

let personSecondId = peoples.get(2)
personSecondId.name = 'Nikola Tesla'
console.log(personSecondId)
    // { id: 2, name: 'Nikola Tesla', money: '9400' }

console.log(peoples.set({ id: 2, name: 'Donald Trump', money: '177000' }))
console.log(peoples.get())
    // [
    //     { id: 1, name: 'John Cina', money: '100' },
    //     { id: 2, name: 'Donald Trump', money: '177000' },
    //     { id: 3, name: 'Jina Ross', money: '450' },
    //     { id: 4, name: 'Riley Reid', money: '1080' }
    // ]

console.log(personSecondId)
    // WARNING !!!
    // { id: 2, name: 'Elon Mask', money: '9400' }

console.log(peoples.get(2))
    // BUT
    // { id: 2, name: 'Donald Trump', money: '177000' }

peoples.clear()
console.log(peoples.get())
    // []
```
